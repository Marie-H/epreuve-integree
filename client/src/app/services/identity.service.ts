import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Identity } from '../models/identity.model';
import { Observable } from 'rxjs';

const baseUrl = 'http://localhost:3000/api/identities';

@Injectable({
  providedIn: 'root'
})
export class IdentityService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(baseUrl);
  }

  getAllFromUser(userid: Number): Observable<Identity[]> {
    // return this.http.get(baseUrl, data);
    return this.http.get<Identity[]>(`${baseUrl}/${userid}`);
  }

  get(id) {
    return this.http.get<Identity>(`${baseUrl + '/getone'}/${id}`);
  }

  create(data: any): Observable<Identity> {
    return this.http.post<Identity>(baseUrl, data);
  }

  addProfile(name: string, game: string, image: File, userid): Observable<Identity> {
    const profileData = new FormData();
    profileData.append("name", name);
    profileData.append("game", game);
    profileData.append("userid", userid);
    profileData.append("image", image, name);
    return this.http
      .post<Identity>(baseUrl, profileData)
  }

  update(id, data: any) {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  updateImage(id, name: string, image: File): Observable<Identity> {
    const profileData = new FormData();
    profileData.append("id", id);
    profileData.append("image", image, name);
    return this.http.put<Identity>(`${baseUrl + '/image'}/${id}`, profileData);
  }

  delete(id: string) {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll() {
    return this.http.delete(baseUrl);
  }

  findByTitle(title: string) {
    return this.http.get(`${baseUrl}?title=${title}`);
  }
}