import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { calendarEvent } from '../models/calendarEvent.model';

const baseUrl = 'http://localhost:3000/api/identities/details/event';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) { }

  getAllFromUser(identityid): Observable<calendarEvent[]> {
    return this.http.get<calendarEvent[]>(`${baseUrl}/${identityid}`);
  }

  create(data: calendarEvent): Observable<calendarEvent> {
    return this.http.post<calendarEvent>(baseUrl, data);
  }

  delete(id: Number): Observable<calendarEvent> {
    return this.http.delete<calendarEvent>(`${baseUrl}/${id}`);
  }

  update(data: calendarEvent): Observable<calendarEvent> {
    return this.http.put<calendarEvent>(baseUrl, data);
  }
}
