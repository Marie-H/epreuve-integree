import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Preferences } from '../models/preferences';

const baseUrl = 'http://localhost:3000/api/preferences';

@Injectable({
  providedIn: 'root'
})
export class PreferencesService {

  constructor(private http: HttpClient) { }

  getPreferences(userid: Number) : Observable<Preferences> {
    return this.http.get<Preferences>(`${baseUrl}/${userid}`);
  }

  update(data: Preferences): Observable<Preferences> {
    return this.http.put<Preferences>(baseUrl, data);
  }



  private subject = new Subject<any>();

    sendMessage(message: string) {
        window.sessionStorage.setItem("language", message);
        this.subject.next({ message });
    }

    clearMessages() {
        this.subject.next();
    }

    onMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
