import { TestBed } from '@angular/core/testing';

import { RefreshFooterService } from './refresh-footer.service';

describe('RefreshFooterService', () => {
  let service: RefreshFooterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RefreshFooterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
