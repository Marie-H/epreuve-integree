import { TestBed } from '@angular/core/testing';

import { EventsDisplayService } from './events-display.service';

describe('EventsDisplayService', () => {
  let service: EventsDisplayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventsDisplayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
