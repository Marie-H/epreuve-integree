import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './services/token-storage.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subscription } from 'rxjs';
import { PreferencesService } from './services/preferences.service';
import { Router } from '@angular/router';
import { Identity } from './models/identity.model';
import { IdentityService } from './services/identity.service';
import { RefreshListService } from './services/refresh-list.service';
import { Title } from '@angular/platform-browser';
import { ThemeService } from './services/theme.service';
import { OverlayContainer } from '@angular/cdk/overlay';
import { RefreshFooterService } from './services/refresh-footer.service';
import { MatDialog } from '@angular/material/dialog';
import { IdentitiesMngtComponent } from './components/identities-mngt/identities-mngt.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username: string = "";
  subscription: Subscription;
  isDarkThemePref: boolean = false;

  identities: Identity[];
  selectedIdentityName;

  constructor(
    private titleService: Title,
    private router: Router,
    private tokenStorageService: TokenStorageService,
    public translate: TranslateService,
    private preferencesService: PreferencesService,
    private identityService: IdentityService,
    private refreshListService: RefreshListService,
    private themeService: ThemeService,
    private overlayContainer: OverlayContainer,
    private refreshFooterService: RefreshFooterService,
    private dialog: MatDialog,
  ) {
    translate.addLangs(['English', 'Français']);
    if (window.sessionStorage.getItem("language") === null) {
      window.sessionStorage.setItem("language", "English");
    } else {
      translate.use(window.sessionStorage.getItem("language"));
    }
    translate.use(window.sessionStorage.getItem("language"));
    this.subscription = this.preferencesService.onMessage().subscribe(message => {
      translate.use(window.sessionStorage.getItem("language"));
      if (window.sessionStorage.getItem("theme") === "Dark") {
        this.isDarkThemePref = true;
      } else {
        this.isDarkThemePref = false;
      }
      this.ngOnInit(); // Replaces the need to reload (window.location.reload()) the whole page.
    });
  }

  ngOnInit(): void {
    if (window.sessionStorage.getItem("theme") == "Dark") {
      this.overlayContainer.getContainerElement().classList.add('dark-theme');
    } else {
      this.overlayContainer.getContainerElement().classList.remove('dark-theme');

    }
    this.titleService.setTitle("App");

    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');

      this.username = user.username;
    }

    // Checks if the setted theme is "Dark"
    if (window.sessionStorage.getItem("theme") === "Dark") {
      this.isDarkThemePref = true;
    }

    this.themeService.isDarkTheme.subscribe(d => this.isDarkThemePref = d);
    if (this.isLoggedIn) {
      this.retrieveIdentities();
      this.refreshListService.notifyObservable$.subscribe(res => {
        if (res.refresh) {
          this.retrieveIdentities();
        }
      })
    }
  }

  retrieveIdentities() {
    this.identityService.getAllFromUser(this.tokenStorageService.getUser().id)
      .subscribe(
        data => {
          this.identities = data;
          window.sessionStorage.setItem("SelectedIdentityId", this.identities[0]._id.toString());
          window.sessionStorage.setItem("SelectedIdentityName", this.identities[0].name.toString());
          this.selectedIdentityName = window.sessionStorage.getItem("SelectedIdentityName");
        },
        error => {
          console.log(error);
        });
  }

  setIdentity(identity) {
    window.sessionStorage.setItem("SelectedIdentityId", identity._id);
    window.sessionStorage.setItem("SelectedIdentityName", identity.name);
    this.selectedIdentityName = window.sessionStorage.getItem("SelectedIdentityName");
    this.refreshFooterService.sendUpdate(identity._id);
  }

  openIdentitiesManagement(identities: Identity[]) {
    const dlg = this.dialog.open(IdentitiesMngtComponent, { data: { identities }, autoFocus: false });
    dlg.afterClosed().subscribe(() => { this.retrieveIdentities(); });
  }

  logout(): void {
    this.tokenStorageService.signOut();

    this.router.navigate(['home'])
      .then(() => {
        window.location.reload();
      });
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }
}