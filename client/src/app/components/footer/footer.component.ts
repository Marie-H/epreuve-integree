import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subscription } from 'rxjs';
import { PreferencesService } from 'src/app/services/preferences.service';
import { RefreshFooterService } from 'src/app/services/refresh-footer.service';
import { ThemeService } from 'src/app/services/theme.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  isLoggedIn = false;
  isDarkTheme: Observable<boolean>;
  languageSettings: string[];
  selectedLanguage: string;
  private subscriptionName: Subscription;

  constructor(
    private tokenStorageService: TokenStorageService,
    private themeService: ThemeService,
    public translate: TranslateService,
    private preferencesService: PreferencesService,
    private refreshFooter: RefreshFooterService,
    private overlayContainer: OverlayContainer) { }

  ngOnInit(): void {
    if (window.sessionStorage.getItem("theme") == "Dark") {
      this.overlayContainer.getContainerElement().classList.add('dark-theme');
    } else {
      this.overlayContainer.getContainerElement().classList.remove('dark-theme');
    }

    // subscribe to sender component messages
    this.subscriptionName = this.refreshFooter.getUpdate().subscribe
      (message => { //message contains the data sent from service
        this.isLoggedIn = true;
      });
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    this.isDarkTheme = this.themeService.isDarkTheme;
    this.languageSettings = this.translate.getLangs();
    this.selectedLanguage = this.languageSettings.find(l => l == window.sessionStorage.getItem("language"));
  }

  toggleDarkTheme(checked: boolean) {
    this.themeService.setDarkTheme(checked);
  }

  changeLanguage() {
    this.preferencesService.sendMessage(this.selectedLanguage);
  }

  ngOnDestroy() { // It's a good practice to unsubscribe to ensure no memory leaks
    this.subscriptionName.unsubscribe();
  }


}
