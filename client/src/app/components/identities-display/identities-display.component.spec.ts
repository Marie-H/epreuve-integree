import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentitiesDisplayComponent } from './identities-display.component';

describe('IdentitiesDisplayComponent', () => {
  let component: IdentitiesDisplayComponent;
  let fixture: ComponentFixture<IdentitiesDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdentitiesDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentitiesDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
