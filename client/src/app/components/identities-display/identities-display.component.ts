import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Identity } from 'src/app/models/identity.model';
import { IdentityService } from 'src/app/services/identity.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-identities-display',
  templateUrl: './identities-display.component.html',
  styleUrls: ['./identities-display.component.css']
})
export class IdentitiesDisplayComponent implements OnInit {
  identities: Identity[];
  displayedColumns: string[] = ['Picture', 'Name', 'Game', 'Options', 'actions'];
  dataSource: MatTableDataSource<Identity>;

  form: FormGroup;
  profile: Identity;
  imageData;

  addMode: boolean = false;

  constructor(private token: TokenStorageService,
    private identityService: IdentityService) { }

  ngOnInit() {
    this.InitIdentities();

    this.form = new FormGroup({
      name: new FormControl(null),
      game: new FormControl(null),
      image: new FormControl(null),
    });

  }

  onFileSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({ image: file });
    const allowedMimeTypes = ["image/png", "image/jpeg", "image/jpg"];
    if (file && allowedMimeTypes.includes(file.type)) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imageData = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  onFileSelectUpdate(event: Event, id, name) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({ image: file });
    const allowedMimeTypes = ["image/png", "image/jpeg", "image/jpg"];
    if (file && allowedMimeTypes.includes(file.type)) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imageData = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
    this.identityService.updateImage(id, name, this.form.value.image).subscribe(
      () => {
        this.refreshIdentities();
      }
    );
  }

  onSubmit() {
    this.identityService.addProfile(this.form.value.name, this.form.value.game, this.form.value.image, this.token.getUser().id).subscribe(
      () => {
        this.refreshIdentities();
      }
    );
    this.form.reset();
    this.imageData = null;
    this.addMode = false;
  }

  deleteIdentity(id) {
    this.identityService.delete(id).subscribe(() => {
      this.refreshIdentities();
    });
  }

  updateIdentity(row: Identity) {
    let data: Identity = {
      _id: row._id,
      name: row.name,
      game: row.game,
      userId: row.userId,
      imagePath: row.imagePath
    };
    this.identityService.update(data._id, data).subscribe(() => {
      this.refreshIdentities()
    });
  }

  InitIdentities() {
    this.identityService.getAllFromUser(this.token.getUser().id).subscribe(
      data => {
        this.identities = data;
        this.dataSource = new MatTableDataSource(this.identities);
      }
    );
  }

  refreshIdentities() {
    this.identityService.getAllFromUser(this.token.getUser().id).subscribe(
      data => {
        this.identities = data;
        this.identities.forEach(i => {
          i.imagePath = i.imagePath + "?" + Date.now();
        })
        this.dataSource = new MatTableDataSource(this.identities);
      }
    );
  }

  toggleAddMode() {
    this.addMode = !this.addMode;
    this.form.reset();
    this.imageData = null;
  }

}
