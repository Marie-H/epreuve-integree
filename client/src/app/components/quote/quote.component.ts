import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.css']
})
export class QuoteComponent implements OnInit {

  url = 'http://quotes.stormconsultancy.co.uk/random.json';
  quoteObject;
  quote;
  author;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.quote = this.http.jsonp(this.url, 'callback').subscribe(d => {
      this.quoteObject = d;
      this.quote = this.quoteObject.quote;
      this.author = this.quoteObject.author
    });
  }

}
