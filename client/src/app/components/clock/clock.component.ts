import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})
export class ClockComponent implements OnInit {

  day = "";
  time = "";
  clockHandle;

  constructor(private overlayContainer: OverlayContainer,) { }

  ngOnInit(): void {
    if (window.sessionStorage.getItem("theme") == "Dark") {
      this.overlayContainer.getContainerElement().classList.add('dark-theme');
    } else {
      this.overlayContainer.getContainerElement().classList.remove('dark-theme');
    }

    this.clockHandle = setInterval(() => {
      this.Heure();
    }, 1000);
  }

  Heure() {
    let jours;
    let mois;
    if (window.sessionStorage.getItem("language") == "English") {
      jours = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saterday");
      mois = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    } else {
      jours = new Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
      mois = new Array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
    }
    let d = new Date();
    let h, min, sec, n, j, m, a;
    if (d.getHours() < 10) { h = "0" + d.getHours() }
    else { h = d.getHours() };
    if (d.getMinutes() < 10) { min = "0" + d.getMinutes() }
    else { min = d.getMinutes() };
    if (d.getSeconds() < 10) { sec = "0" + d.getSeconds() }
    else { sec = d.getSeconds() };
    j = jours[d.getDay()];
    if (d.getDate() < 10) { n = "0" + d.getDate() }
    else { n = d.getDate() }
    m = mois[d.getMonth()];
    a = d.getFullYear();
    this.day = j + " " + n + " " + m + " " + a;
    this.time = + h + ':' + min + ':' + sec;
  }

}
