import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { AppComponent } from 'src/app/app.component';
import { Identity } from 'src/app/models/identity.model';
import { IdentityService } from 'src/app/services/identity.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-identities-mngt',
  templateUrl: './identities-mngt.component.html',
  styleUrls: ['./identities-mngt.component.css']
})
export class IdentitiesMngtComponent implements OnInit {
  identities: Identity[];
  displayedColumns: string[] = ['Name', 'actions'];
  dataSource: MatTableDataSource<Identity>;

  editMode: boolean = false;
  addMode: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<AppComponent>,
    private token: TokenStorageService,
    private identityService: IdentityService,
    @Inject(MAT_DIALOG_DATA) public data: { identities: Identity[] }
  ) {
    this.identities = data.identities;
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.identities);
  }

  updateName(nameValue, c: Identity) {
    let data: Identity = {
      _id: c._id,
      name: nameValue,
      game: c.game,
      userId: this.token.getUser().id,
      imagePath: ""
    };
    this.toggleEditMode();
    this.identityService.update(data._id, data).subscribe(() => { this.identitiesInitialization() });
  }

  addIdentity(value) {
    const data = {
      name: value,
      userid: this.token.getUser().id
    };
    this.identityService.create(data).subscribe(() => {
      this.addMode = !this.addMode;
      this.identitiesInitialization();
    })
  }

  deleteIdentity(id) {
    this.identityService.delete(id).subscribe(() => {
      this.identitiesInitialization();
    })
  }

  identitiesInitialization() {
    this.identityService.getAllFromUser(this.token.getUser().id).subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
    })
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
    this.dataSource.data = null;
    this.dataSource = new MatTableDataSource(this.identities);
  }

  toggleAddMode() {
    this.addMode = !this.addMode;
  }

  close() {
    this.dialogRef.close();
  }

}
