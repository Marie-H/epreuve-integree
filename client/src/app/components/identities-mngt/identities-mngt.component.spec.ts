import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentitiesMngtComponent } from './identities-mngt.component';

describe('IdentitiesMngtComponent', () => {
  let component: IdentitiesMngtComponent;
  let fixture: ComponentFixture<IdentitiesMngtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdentitiesMngtComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentitiesMngtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
