import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { TodoService } from 'src/app/services/todo.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Card } from 'src/app/models/card.model';
import { toDoTab } from 'src/app/models/toDoTab.model';
import { RefreshFooterService } from 'src/app/services/refresh-footer.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.css']
})
export class ToDoComponent implements OnInit, OnChanges {
  identityId;
  todolist: any = [];
  doinglist: any = [];
  donelist: any = [];
  response: any;
  toDoTab: toDoTab;

  constructor(private todoService: TodoService,
    private refreshFooterService: RefreshFooterService,
    private route: ActivatedRoute) { }

  ngOnChanges(): void {
    this.listsInitializations();
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.identityId = params['id'];
    });
    this.listsInitializations();
    this.refreshFooterService.getUpdate().subscribe((data) => {
      this.identityId = data.text; this.listsInitializations();
    })

  }

  onInputEnter(listType: number, cardName: String) {

    const data: Card = {
      name: cardName,
      toDoTab: this.toDoTab,
      type: listType
    };
    this.todoService.create(data)
      .subscribe(
        () => {
          this.listsInitializations();
        },
        error => {
          console.log(error);
        });

  }

  deleteCard(c: Card) {
    this.todoService.delete(c._id).subscribe(() => this.listsInitializations());
  }

  listsInitializations() {
    this.todolist = []; this.doinglist = []; this.donelist = [];
    this.todoService.getAllFromUser(this.identityId).subscribe(data => {
      if (data.length > 0) {
        this.response = data;
        this.toDoTab = this.response[0]._id;
        this.response.forEach(element => {
          element.cards.forEach(ele => {
            if (ele.type == 1) {
              this.todolist.push(ele);
            }
            else if (ele.type == 2) {
              this.doinglist.push(ele);
            }
            else {
              this.donelist.push(ele);
            }
          });
        });
      }
    })
  }

  drop(event: CdkDragDrop<string[]>) {
    let droppedListType: Number;
    if (event.container.id == "cdk-drop-list-0") {
      droppedListType = 1;
    } else if (event.container.id == "cdk-drop-list-1") {
      droppedListType = 2;
    } else {
      droppedListType = 3;
    }
    let cardDropped = event.previousContainer.data[event.previousIndex] as any;

    const data: Card = {
      _id: cardDropped._id,
      name: cardDropped.name,
      toDoTab: this.toDoTab,
      type: droppedListType
    };
    this.todoService.update(data).subscribe(() => { this.listsInitializations(); });

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  updateOnEnter(value: string, card: Card) {
    const data: Card = {
      _id: card._id,
      name: value,
      toDoTab: this.toDoTab,
      type: card.type
    };
    this.todoService.update(data).subscribe(() => { this.listsInitializations(); });
  }

}
