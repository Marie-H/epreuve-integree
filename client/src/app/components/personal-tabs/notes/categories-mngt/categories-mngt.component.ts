import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Category } from 'src/app/models/category.model';
import { CategoryService } from 'src/app/services/category.service';
import { NotesComponent } from '../notes.component';

@Component({
  selector: 'app-categories-mngt',
  templateUrl: './categories-mngt.component.html',
  styleUrls: ['./categories-mngt.component.css']
})
export class CategoriesMngtComponent implements OnInit {

  identity;
  categories: Category[];
  displayedColumns: string[] = ['Name', 'Color', 'actions'];
  dataSource: MatTableDataSource<Category>;
  color = "lightgrey";
  chosenColor: string;
  editMode: boolean = false;
  addMode: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<NotesComponent>,
    private categoryService: CategoryService,
    @Inject(MAT_DIALOG_DATA) public data: { categories: Category[], identity }
  ) {
    this.categories = data.categories;
    this.identity = data.identity;
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.categories);
  }

  constructSrc(hexVal, size) {
    const constructedVal = "https://icongr.am/material/checkbox-blank.svg?size=" + size + "&color=" + hexVal.substring(1);
    return constructedVal;
  }

  addedColor(event: string, data: any, id): void {
    this.chosenColor = data;
  }

  updateColor(color: any, c: Category): void {
    this.chosenColor = color;
    let data: Category = {
      _id: c._id,
      name: c.name,
      hex: c.hex,
      identity: c.identity
    };
    this.toggleEditMode();
    this.categoryService.update(data).subscribe(() => { this.categoriesInitialization() });
  }

  updateName(nameValue, c: Category) {
    let data: Category = {
      _id: c._id,
      name: nameValue,
      hex: c.hex,
      identity: c.identity
    };
    this.toggleEditMode();
    this.categoryService.update(data).subscribe(() => { this.categoriesInitialization() });
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
    this.dataSource.data = null;
    this.dataSource = new MatTableDataSource(this.categories);
  }

  toggleAddMode() {
    this.addMode = !this.addMode;
    this.color = "lightgrey"
  }

  addCategory(value) {
    const data: Category = {
      name: value,
      identity: this.identity,
      hex: this.chosenColor,
    };
    this.categoryService.create(data).subscribe(() => {
      this.addMode = !this.addMode;
      this.categoriesInitialization();
    })
  }

  deleteCategory(id) {
    this.categoryService.delete(id).subscribe(() => {
      this.categoriesInitialization();
    })
  }

  categoriesInitialization() {
    this.categoryService.getAllCategoriesFromIdentity(this.identity).subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
    })
  }

  close() {
    this.dialogRef.close();
  }


}
