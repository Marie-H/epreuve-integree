import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Category } from 'src/app/models/category.model';
import { Identity } from 'src/app/models/identity.model';
import { Note } from 'src/app/models/note.model';
import { CategoryService } from 'src/app/services/category.service';
import { IdentityService } from 'src/app/services/identity.service';
import { NoteService } from 'src/app/services/note.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { NotesComponent } from '../notes.component';

import { OverlayContainer } from '@angular/cdk/overlay';

@Component({
  selector: 'app-note-details',
  templateUrl: './note-details.component.html',
  styleUrls: ['./note-details.component.css']
})
export class NoteDetailsComponent implements OnInit {
  note: Note;
  noteCategories: Category[];
  menuCategories: Category[];
  identityCategories = [];
  identityId;
  identities: Identity[];
  isEditNoteMode: boolean = false;
  updateTitle;
  updateContent;

  constructor(
    public dialogRef: MatDialogRef<NotesComponent>,
    private noteService: NoteService,
    private categoryService: CategoryService,
    private tokenStorageService: TokenStorageService,
    private identityService: IdentityService,
    private overlayContainer: OverlayContainer,
    @Inject(MAT_DIALOG_DATA) public data: { note: any },
  ) {
    this.note = data.note;
    this.identityId = data.note.identity;
    this.noteCategories = this.note.categories;
    this.updateTitle = data.note.name;
    this.updateContent = data.note.content;
  }

  ngOnInit(): void {
    this.categoriesInitialization();
    this.retrieveIdentities();
    if (window.sessionStorage.getItem("theme") == "Dark") {
      this.overlayContainer.getContainerElement().classList.add('dark-theme');
    } else {
      this.overlayContainer.getContainerElement().classList.remove('dark-theme');

    }
  }

  categoriesInitialization() {
    this.categoryService.getAllCategoriesFromIdentity(this.identityId).subscribe(data => {
      this.identityCategories = data;
      this.menuCategories = this.filteredCategories(this.noteCategories);
    })
  }

  retrieveIdentities() {
    this.identityService.getAllFromUser(this.tokenStorageService.getUser().id)
      .subscribe(
        data => {
          this.identities = data;
          this.identities = this.filteredIdentities();
        },
        error => {
          console.log(error);
        });
  }

  constructSrc(hexVal, size) {
    const constructedVal = "https://icongr.am/material/label.svg?size=" + size + "&color=" + hexVal.substring(1);
    return constructedVal;
  }

  filteredCategories(array: any) {
    let a = this.identityCategories.filter((el) => !array.find(a => a.name === el.name));
    return a;
  }

  filteredIdentities() {
    let a = this.identities.filter(a => a._id !== this.identityId);
    return a;
  }

  addCategory(c: Category) {
    this.categoryService.addCategoryToNote(this.note._id, c._id).subscribe(
      () => {
        this.noteService.get(this.note._id).subscribe(data => {
          this.noteCategories = data.categories;
          this.menuCategories = this.filteredCategories(this.noteCategories);
        })
      }
    );
  }

  deleteCategory(c: Category) {
    this.categoryService.deleteCategoryFromNote(this.note._id, c._id).subscribe(
      () => {
        this.noteService.get(this.note._id).subscribe(data => {
          this.noteCategories = data.categories;
          this.categoriesInitialization();
        })
      }
    );
  }

  copyNote(c: Identity) {
    const data: Note = {
      name: this.note.name,
      identity: c,
      content: this.note.content,
      summary: this.note.summary,
      categories: this.note.categories
    };
    this.noteService.create(data).subscribe(() => { this.close() });
  }

  moveNote(c: Identity) {
    const data: Note = {
      name: this.note.name,
      identity: c,
      content: this.note.content,
      summary: this.note.summary,
      categories: this.note.categories
    };
    this.noteService.create(data).subscribe(() => {
      this.deleteNote();
    });
  }

  editNoteMode() {
    this.isEditNoteMode = !this.isEditNoteMode;
  }

  updateNote() {
    this.isEditNoteMode = false;
    const data: Note = {
      _id: this.note._id,
      name: this.updateTitle,
      identity: this.note.identity,
      content: this.updateContent,
      summary: this.note.summary,
      categories: this.note.categories
    };
    this.noteService.update(data).subscribe(() => this.close());
  }

  deleteNote() {
    this.noteService.delete(this.note._id).subscribe(() => {
      this.close();
    })
  }

  isLink(s: String) {
    return s.slice(0, 4) === "http";
  }

  close() {
    this.dialogRef.close();
  }
}
