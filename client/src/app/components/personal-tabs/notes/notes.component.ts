import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { NoteService } from 'src/app/services/note.service';
import { MatDialog } from '@angular/material/dialog';
import { AddNoteComponent } from './add-note/add-note.component';
import { Note } from 'src/app/models/note.model';
import { CategoryService } from 'src/app/services/category.service';
import { NoteDetailsComponent } from './note-details/note-details.component';
import { CategoriesMngtComponent } from './categories-mngt/categories-mngt.component';
import { Category } from 'src/app/models/category.model';
import { OverlayContainer } from '@angular/cdk/overlay';
import { WebSocketService } from 'src/app/services/web-socket.service';
import { RefreshFooterService } from 'src/app/services/refresh-footer.service';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit, OnChanges {
  identityId;
  notes: Note[] = [];
  identityCategories = [];

  filteredCategory: Category;
  inputfilter = "";

  // WebSocket
  connection;
  message;
  //

  constructor(
    private noteService: NoteService,
    private dialog: MatDialog,
    private categoryService: CategoryService,
    private overlayContainer: OverlayContainer,
    private webSocketService: WebSocketService,
    private refreshFooterService: RefreshFooterService,
    private route: ActivatedRoute) { }

  ngOnChanges(): void {
    this.notesInitializations();
    this.categoriesInitialization();
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.identityId = params['id'];
    });

    if (window.sessionStorage.getItem("theme") == "Dark") {
      this.overlayContainer.getContainerElement().classList.add('dark-theme');
    } else {
      this.overlayContainer.getContainerElement().classList.remove('dark-theme');
    }
    this.notesInitializations();
    this.categoriesInitialization();

    // WebSocket
    this.connection = this.webSocketService.getMessages().subscribe(() => {
      this.notesInitializations();
    });

    this.refreshFooterService.getUpdate().subscribe((data) => { this.identityId = data.text; this.notesInitializations(); this.categoriesInitialization(); })
  }

  // sendMessage(){
  //   this.webSocketService.sendMessage(this.message);
  //   this.message = '';
  // }

  constructSrc(hexVal, size) {
    const constructedVal = "https://icongr.am/material/label.svg?size=" + size + "&color=" + hexVal.substring(1);
    return constructedVal;
  }

  notesInitializations() {
    this.noteService.getAllNotesFromIdentity(this.identityId).subscribe(data => {
      this.notes = data;
    })
  }

  categoriesInitialization() {
    this.categoryService.getAllCategoriesFromIdentity(this.identityId).subscribe(data => {
      this.identityCategories = data;
    })
  }

  open() {
    let identity = this.identityId;
    const dlg = this.dialog.open(AddNoteComponent, { data: { identity } });
    dlg.afterClosed().subscribe(() => { this.notesInitializations(); });
  }

  openNote(note: Note) {
    const dlg = this.dialog.open(NoteDetailsComponent, { data: { note }, autoFocus: false });
    dlg.afterClosed().subscribe(() => { this.filterCategory(); });
  }

  openCategoriesManagement(categories: Category[]) {
    let identity = this.identityId;
    const dlg = this.dialog.open(CategoriesMngtComponent, { data: { categories, identity }, autoFocus: false });
    dlg.afterClosed().subscribe(() => { this.notesInitializations(); this.categoriesInitialization(); });
  }

  filterCategory() {
    this.noteService.getAllNotesFromIdentity(this.identityId).subscribe(data => {
      this.notes = data;
      if (this.filteredCategory != null) {
        let filter = this.filteredCategory.name;
        if (filter != "") {
          let filterName: String = this.filteredCategory.name;
          if (this.inputfilter != "") {
            let filteredList = this.notes.filter(n => n.categories.some(nc => nc.name == filterName)).filter(n => n.name === this.inputfilter);
            this.notes = filteredList;
          } else {
            let filteredList = this.notes.filter(n => n.categories.some(nc => nc.name == filterName));
            this.notes = filteredList;
          }
        }
      } else { //no categories, only the input 
        if (this.inputfilter != "") {
          let filteredList = this.notes.filter(n => n.name === this.inputfilter);
          this.notes = filteredList;
        }
      }
    });

  }

}
