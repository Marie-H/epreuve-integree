import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Note } from 'src/app/models/note.model';
import { NoteService } from 'src/app/services/note.service';
import { NotesComponent } from '../notes.component';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.css']
})
export class AddNoteComponent implements OnInit {
  identity;
  name = '';
  summary = '';
  content = '';

  constructor(
    public dialogRef: MatDialogRef<NotesComponent>,
    private noteService: NoteService,
    @Inject(MAT_DIALOG_DATA) public data: { identity: any },
  ) {
    this.identity = data.identity;
  }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    const data: Note = {
      name: this.name,
      identity: this.identity,
      content: this.content,
      summary: this.summary,
      categories : []
    };
    this.noteService.create(data).subscribe(() => { this.close() });

  }

}
