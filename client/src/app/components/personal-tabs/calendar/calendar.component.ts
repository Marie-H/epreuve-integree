import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DateSelectArg, EventClickArg, EventApi } from '@fullcalendar/angular';
import { calendarEvent } from 'src/app/models/calendarEvent.model';
import { EventService } from 'src/app/services/event.service';
import { EventsDisplayService } from 'src/app/services/events-display.service';
import { IdentityService } from 'src/app/services/identity.service';
import { RefreshFooterService } from 'src/app/services/refresh-footer.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, OnChanges {
  identityId;
  identity;

  calendarVisible = false;
  calendarOptions: any;

  currentEvents: EventApi[] = [];

  events: calendarEvent[];

  constructor(
    private eventsDisplay: EventsDisplayService,
    private eventService: EventService,
    private identityService: IdentityService,
    private refreshFooterService: RefreshFooterService,
    private route: ActivatedRoute
  ) { }

  ngOnChanges(): void {
    this.refreshEvents();
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.identityId = params['id'];
      this.identityService.get(this.identityId).subscribe(data => { this.identity = data; this.set(); this.refreshEvents(); });
    });


    this.refreshFooterService.getUpdate().subscribe((data) => {
      this.identityId = data.text;
      this.identityService.get(this.identityId).subscribe(data => { this.identity = data; this.set(); this.refreshEvents(); });
    })
  }

  set() {
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.calendarOptions = { events: null };
      this.calendarOptions = {
        headerToolbar: {
          left: 'prev,next today',
          center: 'title',
          right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
        },
        initialView: 'dayGridMonth',
        events: this.events, // alternatively, use the `events` setting to fetch from a feed
        weekends: true,
        editable: true,
        selectable: true,
        selectMirror: true,
        dayMaxEvents: true,
        firstDay: 1,
        select: this.handleDateSelect.bind(this),
        eventClick: this.handleEventClick.bind(this),
        eventsSet: this.handleEvents.bind(this),
        eventChange: this.update.bind(this),
        /* you can update a remote database when these fire:
        eventAdd:
        eventChange:
        eventRemove:
        */
      };
      this.calendarVisible = true;
    }, 1500);
  }

  handleCalendarToggle() {
    this.calendarVisible = !this.calendarVisible;
  }

  handleWeekendsToggle() {
    const { calendarOptions } = this;
    calendarOptions.weekends = !calendarOptions.weekends;
  }

  handleDateSelect(selectInfo: DateSelectArg) {
    const title = prompt('Please enter a new title for your event');
    const calendarApi = selectInfo.view.calendar;

    calendarApi.unselect(); // clear date selection

    if (title) {
      let e: calendarEvent = {
        title: title,
        start: selectInfo.startStr,
        end: selectInfo.endStr,
        allDay: selectInfo.allDay,
        identity: this.identity
      }
      this.eventService.create(e).subscribe((data) => { if (data) { this.refreshEvents(); } });
    }
  }

  handleEventClick(clickInfo: EventClickArg) {
    if (confirm(`Are you sure you want to delete the event '${clickInfo.event.title}'`)) {
      console.log(clickInfo.event._def.extendedProps._id)
      this.eventService.delete(clickInfo.event._def.extendedProps._id).subscribe((data) => { this.refreshEvents(); });
    }
  }

  update(clickInfo: EventClickArg) {
    let e: calendarEvent = {
      _id: clickInfo.event.extendedProps._id,
      title: clickInfo.event._def.title,
      start: clickInfo.event._instance.range.start.toISOString(),
      end: clickInfo.event._instance.range.end.toISOString(),
      allDay: clickInfo.event._def.allDay,
      identity: clickInfo.event.extendedProps.identity
    }
    this.eventService.update(e).subscribe(() => { });
  }

  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
  }

  refreshEvents() {
    this.eventsDisplay.getData().subscribe((data) => {
      // if (data === "set") {
      this.eventService.getAllFromUser(this.identityId).subscribe((data) => {
        this.events = data;
        this.set();
      })
      // }
    });
  }
}
