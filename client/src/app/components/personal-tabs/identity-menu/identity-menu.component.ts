import { Component, Input, OnInit } from '@angular/core';
import { Identity } from 'src/app/models/identity.model';
import { IdentityService } from 'src/app/services/identity.service';

@Component({
  selector: 'app-identity-menu',
  templateUrl: './identity-menu.component.html',
  styleUrls: ['./identity-menu.component.css']
})
export class IdentityMenuComponent implements OnInit {

  @Input() item;
  identity: Identity;
  constructor(private identityService: IdentityService) { }

  ngOnInit(): void {
    this.identityService.get(this.item).subscribe(d => { this.identity = d; })
  }

}
