import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Preferences } from 'src/app/models/preferences';
import { PreferencesService } from 'src/app/services/preferences.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit {

  languageSettings: string[];
  selectedLanguage: string;

  themeSettings: string[] = ["Light", "Dark"];
  selectedTheme: string;

  form: FormGroup;

  user = this.tokenStorageService.getUser();

  constructor(
    private titleService: Title,
    public translate: TranslateService,
    private tokenStorageService: TokenStorageService,
    private preferencesService: PreferencesService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.translate.get("SETTINGS").subscribe(name => {
      this.titleService.setTitle("App - " + name);
    });
    this.languageSettings = this.translate.getLangs();
    this.selectedSettings();
    this.form = this.fb.group({
      language: window.sessionStorage.getItem("language"),
      theme: window.sessionStorage.getItem("theme")
    });

  }

  submit() {
    const data: Preferences = {
      language: this.form.value.language,
      theme: this.form.value.theme,
      userId: this.tokenStorageService.getUser().id
    };
    this.preferencesService.update(data).subscribe(
      () => {
        this.preferencesService.getPreferences(this.tokenStorageService.getUser().id).subscribe(
          data => {
            window.sessionStorage.setItem("language", data[0].language);
            window.sessionStorage.setItem("theme", data[0].theme);
            this.preferencesService.sendMessage(data[0].language);
            this.selectedSettings();
          }
        );
      }
    );
  }

  selectedSettings() {
    this.selectedLanguage = this.languageSettings.find(l => l == window.sessionStorage.getItem("language"));
    this.selectedTheme = this.themeSettings.find(l => l == window.sessionStorage.getItem("theme"));
  }
}
