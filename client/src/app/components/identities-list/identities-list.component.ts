import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Identity } from 'src/app/models/identity.model';
import { IdentityService } from 'src/app/services/identity.service';
import { RefreshListService } from 'src/app/services/refresh-list.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-identities-list',
  templateUrl: './identities-list.component.html',
  styleUrls: ['./identities-list.component.css']
})
export class IdentitiesListComponent implements OnInit {

  constructor(private identityService: IdentityService,
    private token: TokenStorageService,
    private router: Router,
    private refreshListService: RefreshListService) { }

  identities: Identity[];
  currentIdentity = null;
  currentIndex = -1;
  name = '';

  ngOnInit() {
    this.retrieveIdentities();
    this.refreshListService.notifyObservable$.subscribe(res => {
      if (res.refresh) {
        this.retrieveIdentities();
      }
    })
  }

  retrieveIdentities() {
    this.identityService.getAllFromUser(this.token.getUser().id)
      .subscribe(
        data => {
          this.identities = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList() {
    this.retrieveIdentities();
    this.currentIdentity = null;
    this.currentIndex = -1;
  }

  setActiveIdentity(identity: any, index: any) {
    this.currentIdentity = identity;
    this.currentIndex = index;
  }

  deleteIdentity() {
    this.identityService.delete(this.currentIdentity._id)
      .subscribe(
        response => {
          console.log(response);

          this.retrieveIdentities()
          // this.router.navigate(['/identities']);
        },
        error => {
          console.log(error);
        });
  }

  // removeAllIdentities() {
  //   this.tutorialService.deleteAll()
  //     .subscribe(
  //       response => {
  //         console.log(response);
  //         this.retrieveTutorials();
  //       },
  //       error => {
  //         console.log(error);
  //       });
  // }

  // searchTitle() {
  //   this.tutorialService.findByTitle(this.title)
  //     .subscribe(
  //       data => {
  //         this.tutorials = data;
  //         console.log(data);
  //       },
  //       error => {
  //         console.log(error);
  //       });
  // }
}