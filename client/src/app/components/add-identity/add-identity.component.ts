import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IdentityService } from 'src/app/services/identity.service';
import { RefreshListService } from 'src/app/services/refresh-list.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-add-identity',
  templateUrl: './add-identity.component.html',
  styleUrls: ['./add-identity.component.css']
})
export class AddIdentityComponent implements OnInit {
  identity = {
    name: ''
  };
  submitted = false;

  constructor(private identityService: IdentityService,
    private token: TokenStorageService,
    private router: Router, 
    private refreshListService : RefreshListService) { }

  ngOnInit() {
  }

  saveIdentity() {
    const data = {
      name: this.identity.name,
      userid : this.token.getUser().id
    };
    this.identityService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
          this.router.navigate(['/identities/details/' + response._id]);
        },
        error => {
          console.log(error);
        });
        this.refreshListService.notifyOther({refresh: true});
  }

  newIdentity() {
    this.submitted = false;
    this.identity = {
      name: ''
    };
    
  }
  
}