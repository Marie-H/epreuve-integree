import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  form: FormGroup;
  titleAlert: string = 'This field is required';

  constructor(
    private titleService: Title,
    private translate: TranslateService,
    private authService: AuthService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.translate.get("SIGNUPTITLE").subscribe(name => {
      this.titleService.setTitle("App - " + name);
    });
    this.form = this.fb.group({
      'username': [null, Validators.required],
      'password': [null, [Validators.required, this.checkPassword]],
      'validate': ''
    });
  }

  get username() {
    return this.form.get('username') as FormControl
  }
  get password() {
    return this.form.get('password') as FormControl
  }

  checkPassword(control) {
    let enteredPassword = control.value;
    let passwordCheck = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/;
    return !passwordCheck.test(enteredPassword) && enteredPassword ? { requirements: true } : null;
  }

  getErrorPassword() {
    return this.form.get('password').hasError('required')
      ? 'Field is required (at least eight characters, one uppercase letter and one number)'
      : this.form.get('password').hasError('requirements')
        ? 'Password needs to be at least eight characters, one uppercase letter and one number'
        : '';
  }

  submit(): void {
    const data: any = {
      username: this.form.value.username,
      password: this.form.value.password
    };
    this.authService.register(data).subscribe(
      data => {
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

}