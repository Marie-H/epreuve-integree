import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PreferencesService } from 'src/app/services/preferences.service';
import { RefreshFooterService } from 'src/app/services/refresh-footer.service';
import { AuthService } from '../../services/auth.service';
import { TokenStorageService } from '../../services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  form: FormGroup;
  titleAlert: string = 'This field is required';

  constructor(
    private titleService: Title,
    private router: Router,
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private preferencesService: PreferencesService,
    public translate: TranslateService,
    private refreshFooter: RefreshFooterService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.titleService.setTitle("App - Login");
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }

    this.form = this.fb.group({
      'username': [null, Validators.required],
      'password': [null, Validators.required],
      'validate': ''
    });
  }

  get username() {
    return this.form.get('username') as FormControl
  }
  get password() {
    return this.form.get('password') as FormControl
  }

  submit(): void {
    const data: any = {
      username: this.form.value.username,
      password: this.form.value.password
    };
    this.authService.login(data).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;

        window.sessionStorage.setItem("language", "English");
        this.preferencesService.getPreferences(this.tokenStorage.getUser().id).subscribe(
          data => {
            window.sessionStorage.setItem("language", data[0].language);
            window.sessionStorage.setItem("theme", data[0].theme);
            this.preferencesService.sendMessage(data[0].language);
          }
        );
        // send message to subscribers via observable subject
        this.refreshFooter.sendUpdate('Message from Sender Component to Receiver Component!');
        // this.router.navigate(['home']);
        this.reloadPage();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }

    );
  }

  reloadPage(): void {
    window.location.reload();
  }

}