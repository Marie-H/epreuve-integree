import { Component, Input, OnInit } from '@angular/core';
import { IdentityService } from 'src/app/services/identity.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { EventsDisplayService } from 'src/app/services/events-display.service';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-identity-details',
  templateUrl: './identity-details.component.html',
  styleUrls: ['./identity-details.component.css']
})
export class IdentityDetailsComponent implements OnInit {
  currentIdentityId: Number = null;
  message = '';
  currentIdentity;

  constructor(
    private titleService: Title,
    public translate: TranslateService,
    private identityService: IdentityService,
    private route: ActivatedRoute,
    private eventsDisplay: EventsDisplayService) { }

  ngOnInit() {
    this.translate.get("DETAILS").subscribe(name => {
      this.titleService.setTitle("App - " + name);
    });
    this.message = '';
    // this.currentIdentityId = this.route.snapshot.paramMap.get('id');
    this.route.params.subscribe(params => {
      this.currentIdentityId = params["id"];
      this.identityService.get(this.currentIdentityId).subscribe(data => this.currentIdentity = data.name);
    });
  }

  onChange(event: MatTabChangeEvent) {
    const tab = event.tab.textLabel;
    if (tab === "Events") {
      this.eventsDisplay.setData("set");
    }
  }
}