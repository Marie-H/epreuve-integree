// Basics
import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { authInterceptorProviders } from './helpers/auth.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import interactionPlugin from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';
import timeGridPlugin from '@fullcalendar/timegrid';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ColorPickerModule } from 'ngx-color-picker';
import { HttpClientJsonpModule } from '@angular/common/http';

// App Components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { BoardAdminComponent } from './components/board-admin/board-admin.component';
import { BoardModeratorComponent } from './components/board-moderator/board-moderator.component';
import { BoardUserComponent } from './components/board-user/board-user.component';
import { IdentitiesListComponent } from './components/identities-list/identities-list.component';
import { IdentityDetailsComponent } from './components/identity-details/identity-details.component';
import { AddIdentityComponent } from './components/add-identity/add-identity.component';
import { ToDoComponent } from './components/personal-tabs/to-do/to-do.component';
import { NotesComponent } from './components/personal-tabs/notes/notes.component';
import { CalendarComponent } from './components/personal-tabs/calendar/calendar.component';
import { AddNoteComponent } from './components/personal-tabs/notes/add-note/add-note.component';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { NoteDetailsComponent } from './components/personal-tabs/notes/note-details/note-details.component';
import { CategoriesMngtComponent } from './components/personal-tabs/notes/categories-mngt/categories-mngt.component';
import { FooterComponent } from './components/footer/footer.component';
import { IdentitiesMngtComponent } from './components/identities-mngt/identities-mngt.component';
import { ClockComponent } from './components/clock/clock.component';
import { QuoteComponent } from './components/quote/quote.component';
import { IdentitiesDisplayComponent } from './components/identities-display/identities-display.component';
import { IdentityMenuComponent } from './components/personal-tabs/identity-menu/identity-menu.component';

// External
import { MaterialModule } from './material.module';


FullCalendarModule.registerPlugins([
  dayGridPlugin,
  timeGridPlugin,
  listPlugin,
  interactionPlugin
])


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    BoardAdminComponent,
    BoardModeratorComponent,
    BoardUserComponent,
    IdentitiesListComponent,
    IdentityDetailsComponent,
    AddIdentityComponent,
    ToDoComponent,
    NotesComponent,
    CalendarComponent,
    AddNoteComponent,
    UserSettingsComponent,
    NoteDetailsComponent,
    CategoriesMngtComponent,
    FooterComponent,
    IdentitiesMngtComponent,
    ClockComponent,
    QuoteComponent,
    IdentitiesDisplayComponent,
    IdentityMenuComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    FullCalendarModule, // register FullCalendar with you app
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }
    ),
    ColorPickerModule,
    HttpClientJsonpModule
  ],
  providers: [authInterceptorProviders, Title],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
