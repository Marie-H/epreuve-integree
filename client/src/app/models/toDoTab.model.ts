import { Card } from "./card.model";
import { Identity } from "./identity.model";

export class toDoTab {

    id?: Number;
    name: String;
    identity: Identity;
    cards: Card[];
  
    constructor(data: any) {
      if (data) {
        this.id = data._id;
        this.name = data.name;
        this.identity = data.identity;
        this.cards = data.cards;
      }
    }
  }