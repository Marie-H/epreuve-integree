export class Preferences {

    _id?: Number;
    language: string;
    theme: string;
    userId: string;
  
    constructor(data: any) {
      if (data) {
        this._id = data._id;
        this.language = data.language;
        this.theme = data.theme;
        this.userId = data.userid;
      }
    }
  }