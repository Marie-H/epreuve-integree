import { Category } from "./category.model";
import { Identity } from "./identity.model";

export class Note {

  _id?: Number;
  name: String;
  identity: Identity;
  content: String;
  summary?: String;
  categories: Category[];

  constructor(data: any) {
    if (data) {
      this._id = data.Id;
      this.name = data.Name;
      this.identity = data.identity;
      this.content = data.content;
      this.summary = data.summary;
      this.categories = data.categories;
    }
  }
}