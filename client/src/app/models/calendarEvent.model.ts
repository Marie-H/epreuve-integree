import { DateInput, Identity } from "@fullcalendar/common";

export class calendarEvent {

    _id?: Number;
    title: String;
    start: DateInput;
    end: DateInput;
    identity: Identity;
    allDay: Boolean
  
    constructor(data: any) {
      if (data) {
        this._id = data.Id;
        this.title = data.title;
        this.start = data.start;
        this.end = data.end;
        this.identity = data.identity
        this.allDay = data.allDay
      }
    }
  }