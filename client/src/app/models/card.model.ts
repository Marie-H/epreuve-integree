import { toDoTab } from "./toDoTab.model";

export class Card {

    _id?: Number;
    name: String;
    toDoTab: toDoTab;
    type: Number;
  
    constructor(data: any) {
      if (data) {
        this._id = data.Id;
        this.name = data.Name;
        this.toDoTab = data.toDoTab;
        this.type = data.type;
      }
    }
  }