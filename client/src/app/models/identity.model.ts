export class Identity {

  _id?: Number;
  name: String;
  game: string;
  userId: String;
  imagePath: string;

  constructor(data: any) {
    if (data) {
      this._id = data._id;
      this.name = data.name;
      this.game = data.game;
      this.userId = data.userid;
      this.imagePath = data.imagePath
    }
  }
}