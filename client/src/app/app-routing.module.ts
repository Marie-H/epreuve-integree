import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { BoardUserComponent } from './components/board-user/board-user.component';
import { BoardModeratorComponent } from './components/board-moderator/board-moderator.component';
import { BoardAdminComponent } from './components/board-admin/board-admin.component';
import { IdentitiesListComponent } from './components/identities-list/identities-list.component';
import { IdentityDetailsComponent } from './components/identity-details/identity-details.component';
import { AddIdentityComponent } from './components/add-identity/add-identity.component';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { NotesComponent } from './components/personal-tabs/notes/notes.component';
import { ToDoComponent } from './components/personal-tabs/to-do/to-do.component';
import { CalendarComponent } from './components/personal-tabs/calendar/calendar.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'user', component: BoardUserComponent },
  { path: 'mod', component: BoardModeratorComponent },
  { path: 'admin', component: BoardAdminComponent },
  { path: 'notes/:id', component: NotesComponent },
  { path: 'todos/:id', component: ToDoComponent },
  { path: 'calendar/:id', component: CalendarComponent },

  // {
  //   // path: 'identities', component: IdentitiesListComponent,
  //   // children: [
  //   //   // {
  //   //   //   path: 'details/:id',
  //   //   //   // outlet: 'sidemenu',
  //   //   //   component: IdentityDetailsComponent
  //   //   // },
  //   //   {
  //   //     path : 'add',
  //   //     component : AddIdentityComponent
  //   //   }
  //   // ]

  // },
  { path: 'identities/add', component: AddIdentityComponent },
  { path: 'identities/details/:id', component: IdentityDetailsComponent },
  { path: 'settings', component: UserSettingsComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
