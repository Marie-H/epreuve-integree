const mongoose = require("mongoose");

const Card = mongoose.model(
    "Card",
    new mongoose.Schema({
        name: String,
        toDoTab:
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "ToDoTab"
        },
        type: Number

    })
);

module.exports = Card;