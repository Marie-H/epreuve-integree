const mongoose = require("mongoose");

const Event = mongoose.model(
    "Event",
    new mongoose.Schema({
        title: String,
        start: Date,
        end: Date,
        allDay: Boolean,
        identity:
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Identity"
        },
    })
);

module.exports = Event;