const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");
db.identity = require("./identity.model");
db.card = require("./card.model");
db.toDoTab = require("./toDoTab.model");
db.note = require("./note.model");
db.category = require("./category.model")
db.event = require("./event.model");
db.preferences = require("./preferences.model");

db.ROLES = ["user", "admin", "moderator"];

module.exports = db;