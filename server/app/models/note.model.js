const mongoose = require("mongoose");

const Note = mongoose.model(
    "Note",
    new mongoose.Schema({
        name: String,
        summary: String,
        content: String,
        identity:
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Identity"
        },
        categories:
            [{
                type: mongoose.Schema.Types.ObjectId,
                ref: "Category"
            }],
    })
);

module.exports = Note;