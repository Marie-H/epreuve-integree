const mongoose = require("mongoose");

const ToDoTab = mongoose.model(
    "ToDoTab",
    new mongoose.Schema({
        name: String,
        identity:
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Identity"
        },
        cards: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Card"
        }]

    })
);

module.exports = ToDoTab;