const mongoose = require("mongoose");

const Category = mongoose.model(
    "Category",
    new mongoose.Schema({
        name: String,
        hex: String,
        identity:
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Identity"
        },
    },
    )
);

module.exports = Category;