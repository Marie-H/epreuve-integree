const mongoose = require("mongoose");

const Identity = mongoose.model(
    "Identity",
    new mongoose.Schema({
        name: String,
        imagePath: String,
        game: String,
        userid:
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        }

    })
);

module.exports = Identity;