const mongoose = require("mongoose");

const Preferences = mongoose.model(
    "Preferences",
    new mongoose.Schema({
        language: String,
        theme: String,
        user:
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        }

    })
);

module.exports = Preferences;