module.exports = app => {
    const toDoTab = require("../controllers/list.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    app.post("/api/identities/details/list/", toDoTab.create);
  
    // Retrieve all Tutorials
    app.get("/api/identities/details/list/:identityid", toDoTab.findToDoTabFromIdentity);
  
    // // Retrieve all published Tutorials
    // router.get("/published", identities.findAllPublished);
  
    // // Retrieve a single Tutorial with id
    // router.get("/api/identities/getone/:id", identities.findOne);
  
    // Update a Tutorial with id
    app.put("/api/identities/details/list/", toDoTab.update);
  
    // Delete a Tutorial with id
    app.delete("/api/identities/details/list/:id", toDoTab.delete);
  
    // // Create a new Tutorial
    // router.delete("/", identities.deleteAll);
  
    app.use('/api/identities/details/list', router);
  };