module.exports = app => {
  const identities = require("../controllers/identity.controller.js");
  const storage = require('../helpers/storage');

  var router = require("express").Router();

  // Create a new Tutorial
  app.post("/api/identities", storage, identities.create);

  // Retrieve all Tutorials
  app.get("/api/identities/:userid", storage, identities.findAllFromUser);

  // Retrieve all published Tutorials
  router.get("/published", identities.findAllPublished);

  // Retrieve a single Tutorial with id
  app.get("/api/identities/getone/:id", storage, identities.findOne);

  // Update a Tutorial with id
  router.put("/:id", identities.update);

  router.put("/image/:id", storage, identities.updateImage);

  // Delete a Tutorial with id
  app.delete("/api/identities/:id", identities.delete);

  // Create a new Tutorial
  router.delete("/", identities.deleteAll);

  app.use('/api/identities', router);
};