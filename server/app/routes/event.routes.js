module.exports = app => {
    const event = require("../controllers/event.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    app.post("/api/identities/details/event/", event.create);
  
    // Retrieve all Tutorials
    app.get("/api/identities/details/event/:identityid", event.findEventsFromIdentity);
  
    // // Retrieve all published Tutorials
    // router.get("/published", identities.findAllPublished);
  
    // // Retrieve a single Tutorial with id
    // router.get("/api/identities/getone/:id", identities.findOne);
  
    // Update a Tutorial with id
    app.put("/api/identities/details/event/", event.update);
  
    // Delete a Tutorial with id
    app.delete("/api/identities/details/event/:id", event.delete);
  
    // // Create a new Tutorial
    // router.delete("/", identities.deleteAll);
  
    app.use('/api/identities/details/event', router);
  };