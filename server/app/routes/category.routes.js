module.exports = app => {
  const categories = require("../controllers/category.controller");

  var router = require("express").Router();

  // Create
  app.post("/api/identities/details/categories/", categories.create);

  // Update
  app.put("/api/identities/details/categories/", categories.update);

  // Delete
  app.delete("/api/identities/details/categories/:id", categories.delete);

  // Get All
  app.get("/api/identities/details/categories/:identityid", categories.findCategoriesFromIdentity);

  app.get("/api/identities/details/categories/:noteid/:categoryid", categories.addCategoryToNote);

  app.delete("/api/identities/details/categories/:noteid/:categoryid", categories.deleteCategoryFromNote);

  // // Retrieve all published Tutorials
  // router.get("/published", identities.findAllPublished);

  // // Retrieve a single Tutorial with id
  // router.get("/api/identities/getone/:id", identities.findOne);

  // // Update a Tutorial with id
  // router.put("/:id", identities.update);

  // Delete
  // app.delete("/api/identities/details/categories/:id", notes.delete);

  // // Create a new Tutorial
  // router.delete("/", identities.deleteAll);

  app.use('/api/identities/details/note', router);
};