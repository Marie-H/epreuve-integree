module.exports = app => {
  const notes = require("../controllers/note.controller.js");

  var router = require("express").Router();

  // Create a new Tutorial
  app.post("/api/identities/details/note/", notes.create);

  // Retrieve all Tutorials
  app.get("/api/identities/details/note/:identityid", notes.findNotesFromIdentity);

  app.get("/api/identities/details/note/getone/:id", notes.findOne);

  // // Retrieve all published Tutorials
  // router.get("/published", identities.findAllPublished);

  // // Retrieve a single Tutorial with id
  // router.get("/api/identities/getone/:id", identities.findOne);

  // // Update a Tutorial with id
  app.put("/api/identities/details/note/", notes.update);

  // Delete a Tutorial with id
  app.delete("/api/identities/details/note/:id", notes.delete);

  // // Create a new Tutorial
  // router.delete("/", identities.deleteAll);

  app.use('/api/identities/details/note', router);
};