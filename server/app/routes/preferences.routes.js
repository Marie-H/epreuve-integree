module.exports = app => {
    const preferences = require("../controllers/preferences.controller");
  
    var router = require("express").Router();
  
    // // Create a new Tutorial
    // app.post("/api/identities", identities.create);
  
    // Retrieve all Tutorials
    app.get("/api/preferences/:userid", preferences.findPreferences);
  
    // // Retrieve all published Tutorials
    // router.get("/published", identities.findAllPublished);
  
    // // Retrieve a single Tutorial with id
    // router.get("/api/identities/getone/:id", identities.findOne);
  
    // // Update a Tutorial with id
    app.put("/api/preferences", preferences.update);
  
    // // Delete a Tutorial with id
    // app.delete("/api/identities/:id", identities.delete);
  
    // // Create a new Tutorial
    // router.delete("/", identities.deleteAll);
  
    // app.use('/api/identities', router);
  };