const { toDoTab, card } = require("../models");
const db = require("../models");
const ToDoTab = db.toDoTab;
const Card = db.card;
const Event = db.event;

// Retrieve all events from the database.
exports.findEventsFromIdentity = (req, res) => {
    const identityid = req.params.identityid;
    Event.find({ identity: identityid })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Events."
            });
        });
};

exports.create = async (req, res) => {
    // Validate request
    if (!req.body.title) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }
    // Create Event
    const event = new Event({
        title: req.body.title,
        identity: req.body.identity,
        start : req.body.start,
        end : req.body.end,
        allDay : req.body.allDay
    });
    
    // Save Event in the database
    event
        .save(event)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Event."
            });
        });
};

exports.delete = async (req, res) => {

    Event.findByIdAndRemove(req.params.id)
    .then(data => {
        if (!data) {
            res.status(404).send({
                message: `Cannot delete Event with id=${id}. Maybe Event was not found!`
            });
        } else {
            res.send({
                message: "Event was deleted successfully!"
            });
        }
    })
    .catch(err => {
        res.status(500).send({
            message: "Could not delete Event with id=" + id
        });
    });
};

exports.update = async (req, res) => {
    // Validate request
    if (!req.body.title) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }
    Event.findByIdAndUpdate(req.body._id, { start: req.body.start, end : req.body.end })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while updating the Event."
            });
        });
};

// Find a single Tutorial with an id
exports.findOne = (req, res) => {
    const identityid = req.params.id;

};
