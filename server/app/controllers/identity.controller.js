const db = require("../models");
const ToDoTab = require("../models/toDoTab.model");
const Identity = db.identity;
const Card = require("../models/card.model");
const Category = require("../models/category.model");
const Note = require("../models/note.model");
const Event = require("../models/event.model");

exports.create = (req, res) => {

    // Validate request
    if (!req.body.name) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }
    const imagePatha = 'http://localhost:3000/images/' + req.file.filename;

    // Create Udentity
    const identity = new Identity({
        name: req.body.name,
        userid: req.body.userid,
        game: req.body.game,
        imagePath: imagePatha
    });

    // Save Identity in the database
    identity
        .save(identity)
        .then(data => {
            // Create a ToDoTab to go with the new Identity
            const tdt = new ToDoTab({
                identity: identity._id,
                cards: []
            });
            tdt.save(tdt);
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Identity."
            });
        });
};

// Retrieve all identities linked to a user from the database.
exports.findAllFromUser = (req, res) => {
    const userid = req.params.userid;
    Identity.find({ userid: userid })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving identities linked."
            });
        });
};

// Find a single Tutorial with an id
exports.findOne = (req, res) => {
    const identityid = req.params.id;
    Identity.findById(identityid)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred."
            });
        });

};

// Update a Tutorial by the id in the request
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    Identity.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Identity with id=${id}. Maybe Identity was not found!`
                });
            } else res.send({ message: "Identity was updated successfully." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Identity with id=" + id
            });
        });
};

exports.updateImage = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }
    const id = req.params.id;
    let imagePatha = 'http://localhost:3000/images/' + req.file.filename;

    Identity.findByIdAndUpdate(id, { imagePath: imagePatha })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Identity with id=${id}. Maybe Identity was not found!`
                });
            } else
                res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Identity with id=" + id
            });
        });
};

exports.delete = async (req, res) => {
    const id = req.params.id;

    ToDoTab.findOne({ identity: id }).then(
        data => {
            data.cards.forEach(c => {
                Card.findOne({ _id: c }).then(
                    d => Card.deleteOne(d)
                )
            });

        }
    )

    ToDoTab.findOneAndRemove({ identity: id }).then(
    );

    Category.find({ identity: id }).then(
        data => {
            data.forEach(c => {
                Category.findOne({ _id: c._id }).then(
                    cat => Category.deleteOne(cat)
                )
            })
        }
    );

    Note.find({ identity: id }).then(
        data => {
            data.forEach(n => {
                Note.findOne({ _id: n._id }).then(
                    note => Note.deleteOne(note)
                )
            })
        }
    );

    Event.find({ identity: id }).then(
        data => {
            data.forEach(e => {
                Event.findOne({ _id: e._id }).then(
                    ev => Event.deleteOne(ev)
                )
            })
        }
    );

    Identity.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete Identity with id=${id}. Maybe Identity was not found!`
                });
            } else {
                res.send({
                    message: "Identity was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Identity with id=" + id
            });
        });
};

// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {

};

// Find all published Tutorials
exports.findAllPublished = (req, res) => {

};