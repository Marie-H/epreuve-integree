const db = require("../models");
const Prefs = db.preferences;

exports.findPreferences = (req, res) => {
    const userid = req.params.userid;
    Prefs.find({ user: userid })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving preferences."
            });
        });
};

exports.update = async (req, res) => {

    Prefs.findOne({ user: req.body.userId })
        .then(data => {
            Prefs.findByIdAndUpdate(data._id, { language: req.body.language, theme: req.body.theme })
                .then(data => {
                    res.send(data);
                })
                .catch(err => {
                    res.status(500).send({
                        message:
                            err.message || "Some error occurred while updating the Preferences."
                    });
                });
        });
};