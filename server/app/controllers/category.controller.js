const db = require("../models");
const Category = db.category;
const Note = db.note;

exports.findCategoriesFromIdentity = (req, res) => {
    const identityid = req.params.identityid;
    Category.find({ identity: identityid })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Categories."
            });
        });
};

exports.addCategoryToNote = (req, res) => {
    const noteid = req.params.noteid;
    const categoryid = req.params.categoryid;

    Note.findOne({ _id: noteid })
        .then(note => {
            note.categories.push(categoryid);
            note.save();
            res.send();

        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while adding Category."
            });
        });
};

exports.deleteCategoryFromNote = (req, res) => {
    const noteid = req.params.noteid;
    const categoryid = req.params.categoryid;

    Note.findOne({ _id: noteid })
        .then(note => {
            index = note.categories.indexOf(categoryid);
            note.categories.splice(index, 1);
            note.save();
            res.send();
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while adding Category."
            });
        });
};

exports.create = async (req, res) => {
    if (!req.body.name) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }
    // Create Category
    const cat = new Category({
        name: req.body.name,
        hex: req.body.hex,
        identity: req.body.identity
    });

    // Save Category in the database
    cat
        .save(cat)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Category."
            });
        });
};


exports.update = async (req, res) => {

    Category.findOne({ _id: req.body._id })
        .then(data => {

            Category.findByIdAndUpdate(data._id, { name: req.body.name, hex: req.body.hex })
                .then(data => {
                    res.send(data);
                })
                .catch(err => {
                    res.status(500).send({
                        message:
                            err.message || "Some error occurred while updating the Preferences."
                    });
                });
        });
};

exports.delete = async (req, res) => {
    const catId = req.params.id;

    // Delete all references to the soon to be deleted category
    Note.find()
        .then(data => {
            data.forEach(d => {
                d.categories.forEach(c => {
                    if (c == catId) {
                        d.categories.splice(d.categories.indexOf(catId), 1);
                        d.save();
                    }
                })
            }
            )
        }
        );

    Category.findByIdAndRemove(catId)
        .then(
            () => { res.send(); }
        )
};



