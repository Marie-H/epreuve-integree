const db = require("../models");
const Note = db.note;

exports.findNotesFromIdentity = (req, res) => {
    const identityid = req.params.identityid;
    Note.find({ identity: identityid }).populate('categories')
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving toDoTab."
            });
        });
};

exports.findOne = (req, res) => {
    const noteid = req.params.id;
    Note.findOne({ _id: noteid }).populate('categories')
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred."
            });
        });

};

exports.create = async (req, res) => {
    // Validate request
    if (!req.body.name) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }
    // Create Note
    const note = new Note({
        name: req.body.name,
        summary: req.body.summary,
        content: req.body.content,
        identity: req.body.identity
    });

    note
        .save(note)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Note."
            });
        });
};

exports.update = async (req, res) => {
    Note.findOne({ _id: req.body._id })
        .then(data => {
            Note.findByIdAndUpdate(data._id, { name: req.body.name, content: req.body.content })
                .then(data => {
                    res.send(data);
                })
                .catch(err => {
                    res.status(500).send({
                        message:
                            err.message || "Some error occurred while updating the Note."
                    });
                });
        });
};

exports.delete = async (req, res) => {

    Note.findByIdAndRemove(req.params.id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete Note with id=${id}. Maybe Note was not found!`
                });
            } else {
                res.send({
                    message: "Note was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Note with id=" + id
            });
        });
};

