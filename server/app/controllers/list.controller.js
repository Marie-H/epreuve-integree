const { toDoTab, card } = require("../models");
const db = require("../models");
const ToDoTab = db.toDoTab;
const Card = db.card;

// Retrieve all identities linked to a user from the database.
exports.findToDoTabFromIdentity = (req, res) => {
    const identityid = req.params.identityid;
    ToDoTab.find({ identity: identityid }).populate('cards')
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving toDoTab."
            });
        });
};

exports.create = async (req, res) => {
    // Validate request
    if (!req.body.name) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }
    // Create Card
    const card = new Card({
        name: req.body.name,
        toDoTab: req.body.toDoTab,
        type : req.body.type
    });

    var t = await ToDoTab.findById(req.body.toDoTab);
    t.cards.push(card);
    t.save();
    
    // Save Identity in the database
    card
        .save(card)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Card."
            });
        });
};

exports.delete = async (req, res) => {

    cardToFind = await Card.findById(req.params.id);
    foundTodotab = await ToDoTab.findById(cardToFind.toDoTab);

    Card.findByIdAndRemove(req.params.id)
    .then(data => {
        if (!data) {
            res.status(404).send({
                message: `Cannot delete Card with id=${id}. Maybe Card was not found!`
            });
        } else {
            foundTodotab.cards.pull(req.params.id);
            foundTodotab.save();
            res.send({
                message: "Card was deleted successfully!"
            });
        }
    })
    .catch(err => {
        res.status(500).send({
            message: "Could not delete Card with id=" + id
        });
    });
};

exports.update = async (req, res) => {
    // Validate request
    if (!req.body.name) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }
    Card.findByIdAndUpdate(req.body._id, { type: req.body.type, name : req.body.name })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while updating the Card."
            });
        });
};

// Find a single Tutorial with an id
exports.findOne = (req, res) => {
    const identityid = req.params.id;

};
