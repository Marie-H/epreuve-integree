
chrome.contextMenus.removeAll(function () {
    chrome.contextMenus.create({
        title: "App - Create Note",
        id: 'MyItem',
        contexts: ["selection"]
    });

    chrome.contextMenus.create({
        title: "App - Create Link Note",
        id: 'Link',
        contexts: ["all"]
    });
});


chrome.contextMenus.onClicked.addListener(function (clickData) {
    if (clickData.menuItemId == "Link") {
        chrome.tabs.query({
            active: true,
            currentWindow: true
        }, ([currentTab]) => {
            window.localStorage.setItem("text", currentTab.url);
        });
        window.open("index.html", "extension_popup", "width=500, height=600, top=0, left=960, scrollbars=yes,resizable=no");
    } else {
        self.close("index.html");
    }

    if (clickData.menuItemId == "MyItem" && clickData.selectionText) {
        window.localStorage.setItem("text", clickData.selectionText);
        window.open("index.html", "extension_popup", "width=500, height=600, top=0, left=960, scrollbars=yes,resizable=no");
    } else {
        self.close("index.html");
    }
})