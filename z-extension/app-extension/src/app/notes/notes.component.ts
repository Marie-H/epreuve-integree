import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Identity } from '../models/identity.model';
import { Note } from '../models/note.model';
import { IdentityService } from '../services/identity.service';
import { NoteService } from '../services/note.service';
import { WebSocketService } from '../services/web-socket.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit, OnChanges {

  userId: any;
  identities: Identity[] = [];
  selectedIdentity!: Identity;

  isThere: boolean = false;
  selected: any;
  title: string = "";

  inputtext: string = "";

  constructor(
    private identityService: IdentityService,
    private noteService: NoteService,
    private router: Router,
    private webSocketService: WebSocketService
  ) { }


  ngOnChanges(changes: SimpleChanges): void {
    this.inputtext = window.sessionStorage.getItem("text");
    this.inputtext = "";
  }

  ngOnInit(): void {
    this.inputtext = "";
    this.inputtext = window.localStorage.getItem("text");

    if (window.localStorage.getItem("userId")) {
      this.isThere = true;
    } else {
      this.isThere = false;
    }
    this.userId = window.localStorage.getItem("userId");
    this.identityService.getAllFromUser(this.userId).subscribe(data => {
      this.identities = data;
      this.selected = this.identities[0].name;
    })
  }

  save() {
    let t = "";
    if (this.title.length == 0) {
      t = this.inputtext.substring(0, 5);
    } else {
      t = this.title;
    }

    const data: Note = {
      name: t,
      identity: this.identities.find(a => a.name == this.selected),
      content: this.inputtext,
      summary: "",
      categories: []
    };
    this.noteService.create(data).subscribe(() => {
      this.webSocketService.sendMessage('create');
      window.close();
    });

  }

  logout(): void {
    window.sessionStorage.clear();
    window.localStorage.clear();
    this.isThere = false;

    this.router.navigate(['/home']);
  }

}
