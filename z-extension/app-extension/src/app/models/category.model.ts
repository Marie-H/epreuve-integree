import { Identity } from "./identity.model";

export class Category {

  _id?: Number;
  name: String;
  identity: Identity;
  hex: String;

  constructor(data: any) {
    if (data) {
      this._id = data.Id;
      this.name = data.Name;
      this.identity = data.identity;
      this.hex = data.hex;
    }
  }
}