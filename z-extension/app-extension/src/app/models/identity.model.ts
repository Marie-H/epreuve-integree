export class Identity {

    _id?: Number;
    name: String = "";
    userId: String = "";
  
    constructor(data: any) {
      if (data) {
        this._id = data._id;
        this.name = data.name;
        this.userId = data.userid;
      }
    }
  }