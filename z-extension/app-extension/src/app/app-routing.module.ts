import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { NotesComponent } from './notes/notes.component';

const routes: Routes = [
  { path: 'home', pathMatch: 'full', component: AppComponent },
  { path: 'notes', pathMatch: 'full', component: NotesComponent },
  // { path: '', redirectTo: 'home', pathMatch: 'full' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
