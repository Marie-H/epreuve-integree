import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../models/category.model';
import { Note } from '../models/note.model';

const baseUrl = 'https://mhtfe2021.herokuapp.com/api/identities/details/categories';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  getAllCategoriesFromIdentity(identityid: any): Observable<Category[]> {
    return this.http.get<Category[]>(`${baseUrl}/${identityid}`);
  }

  addCategoryToNote(noteid: any, category: any): Observable<Note> {
    return this.http.get<Note>(`${baseUrl}/${noteid}/${category}`);
  }

  deleteCategoryFromNote(noteid: any, category: any): Observable<Note> {
    return this.http.delete<Note>(`${baseUrl}/${noteid}/${category}`);
  }

  create(data: Category): Observable<Category> {
    return this.http.post<Category>(baseUrl, data);
  }

  update(data: Category): Observable<Category> {
    return this.http.put<Category>(baseUrl, data);
  }

  delete(id: Number): Observable<Category> {
    return this.http.delete<Category>(`${baseUrl}/${id}`);
  }


}
