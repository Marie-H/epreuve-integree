import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  private url = 'https://mhtfe2021.herokuapp.com';
  private socket = io(this.url);
  constructor() { }

  sendMessage(message) {
    this.socket.emit('add-message', message);
  }

  // getMessages() {
  //   let observable = new Observable(observer => {
  //     this.socket = io(this.url);
  //     this.socket.on('message', (data) => {
  //       observer.next(data);    
  //     });
  //     // return () => {
  //     //   this.socket.disconnect();
  //     // };  
  //   })     
  //   return observable;
  // }

}
