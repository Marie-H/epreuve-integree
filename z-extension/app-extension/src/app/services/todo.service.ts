import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Card } from '../models/card.model';
import { Identity } from '../models/identity.model';

const baseUrl = 'http://localhost:3000/api/identities/details/list';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private http: HttpClient) { }

  getAllFromUser(identityid: Number): Observable<Identity[]> {
    return this.http.get<Identity[]>(`${baseUrl}/${identityid}`);
  }

  create(data: Card): Observable<Card> {
    return this.http.post<Card>(baseUrl, data);
  }

  delete(id: Number): Observable<Card> {
    return this.http.delete<Card>(`${baseUrl}/${id}`);
  }

  update(data: Card): Observable<Card> {
    return this.http.put<Card>(baseUrl, data);
  }
}
