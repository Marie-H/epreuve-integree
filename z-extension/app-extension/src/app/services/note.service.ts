import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Note } from '../models/note.model';

const baseUrl = 'https://mhtfe2021.herokuapp.com/api/identities/details/note';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  constructor(private http: HttpClient) { }

  getAllNotesFromIdentity(identityid: any): Observable<Note[]> {
    return this.http.get<Note[]>(`${baseUrl}/${identityid}`);
  }

  get(id: any) {
    return this.http.get<Note>(`${baseUrl + '/getone'}/${id}`);
  }

  create(data: Note): Observable<Note> {
    return this.http.post<Note>(baseUrl, data);
  }

  delete(id: Number): Observable<Note> {
    return this.http.delete<Note>(`${baseUrl}/${id}`);
  }
}
