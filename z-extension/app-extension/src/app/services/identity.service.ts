import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Identity } from '../models/identity.model';
import { Observable } from 'rxjs';

const baseUrl = 'https://mhtfe2021.herokuapp.com/api/identities';

@Injectable({
  providedIn: 'root'
})
export class IdentityService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(baseUrl);
  }

  getAllFromUser(userid: Number) : Observable<Identity[]> {
    // return this.http.get(baseUrl, data);
    return this.http.get<Identity[]>(`${baseUrl}/${userid}`);
  }

  get(id: Number) {
    return this.http.get<Identity>(`${baseUrl + '/getone'}/${id}`);
  }

  create(data: any) : Observable<Identity> {
    return this.http.post<Identity>(baseUrl, data);
  }

  update(id: string, data: any) {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: string) {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll() {
    return this.http.delete(baseUrl);
  }

  findByTitle(title: string) {
    return this.http.get(`${baseUrl}?title=${title}`);
  }
}