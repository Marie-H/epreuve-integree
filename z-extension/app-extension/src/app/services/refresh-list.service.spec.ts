import { TestBed } from '@angular/core/testing';

import { RefreshListService } from './refresh-list.service';

describe('RefreshListService', () => {
  let service: RefreshListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RefreshListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
