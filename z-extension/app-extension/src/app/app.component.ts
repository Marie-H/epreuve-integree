import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { TokenStorageService } from './services/token-storage.service';
// import { TranslateService } from '@ngx-translate/core';
// import { PreferencesService } from 'src/app/services/preferences.service';
// import { AuthService } from '/services/auth.service';
// import { TokenStorageService } from '../../services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = "App - Notes";

  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  user: any;

  form!: FormGroup;
  titleAlert: string = 'This field is required';

  isThere = false;


  constructor(
    private titleService: Title,
    private router: Router,
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    // private preferencesService: PreferencesService,
    // public translate: TranslateService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.titleService.setTitle("App - Notes");
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }

    this.form = this.fb.group({
      'username': [null, Validators.required],
      'password': [null, Validators.required],
      'validate': ''
    });

    if (window.localStorage.getItem("userId")) {
      this.isThere = true;
    }
  }

  get username() {
    return this.form.get('username') as FormControl
  }
  get password() {
    return this.form.get('password') as FormControl
  }

  submit(): void {
    const data: any = {
      username: this.form.value.username,
      password: this.form.value.password
    };
    this.authService.login(data).subscribe(
      (data: { accessToken: any; }) => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        this.user = this.tokenStorage.getUser();
        window.sessionStorage.setItem("userId", this.user.id);
        window.localStorage.setItem("userId", this.user.id);

        const id = this.user.id;
        chrome.storage.local.set({ key: id }, function () {

        });

        window.sessionStorage.setItem("language", "English");
        // this.preferencesService.getPreferences(this.tokenStorage.getUser().id).subscribe(
        //   (          data: { language: any; }[]) => {
        //     window.sessionStorage.setItem("language", data[0].language);
        //     window.sessionStorage.setItem("theme", data[0].theme);
        //     this.preferencesService.sendMessage(data[0].language);
        //   }
        // );
        this.router.navigate(['/notes']);
      },
      (err: { error: { message: string; }; }) => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }

    );
  }

  reloadPage(): void {
    window.location.reload();
  }

}
